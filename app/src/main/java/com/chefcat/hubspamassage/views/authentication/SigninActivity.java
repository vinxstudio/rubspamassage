package com.chefcat.hubspamassage.views.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.chefcat.hubspamassage.R;
import com.chefcat.hubspamassage.views.MainDrawerActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SigninActivity extends AppCompatActivity {

    @OnClick(R.id.back)
    public void back(){
        finish();
    }

    @OnClick(R.id.btn_next)
    public void goToMain() {
        Intent i = new Intent(SigninActivity.this, MainDrawerActivity.class);
//        Intent i = new Intent(SigninActivity.this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        ButterKnife.bind(this);
    }
}
