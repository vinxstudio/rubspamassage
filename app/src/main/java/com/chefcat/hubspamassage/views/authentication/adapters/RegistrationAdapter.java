package com.chefcat.hubspamassage.views.authentication.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;

import com.chefcat.hubspamassage.views.fragments.AddInformationFragment;
import com.chefcat.hubspamassage.views.fragments.AddImagesFragment;
import com.chefcat.hubspamassage.views.fragments.SuccessFragment;
import com.chefcat.hubspamassage.views.fragments.ConfirmationFragment;


public class RegistrationAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 4;
    private AppCompatActivity act;

    public RegistrationAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setActivity(AppCompatActivity act){
        this.act = act;

    }

    private AddInformationFragment addInformationFragment;
    private AddImagesFragment addImagesFragment;
    private ConfirmationFragment confirmationFragment;
    private SuccessFragment successFragment;

    public void setTabs(
            AddInformationFragment addInformationFragment,
            AddImagesFragment addImagesFragment,
            ConfirmationFragment confirmationFragment,
            SuccessFragment successFragment){

        this.successFragment = successFragment;
        this.confirmationFragment = confirmationFragment;
        this.addImagesFragment = addImagesFragment;
        this.addInformationFragment = addInformationFragment;

    }

    @Override
    public Fragment getItem(int arg0) {
        switch (arg0) {
            case 0:
                return addInformationFragment;
            case 1:
                return addImagesFragment;
            case 2:
                return confirmationFragment;
            case 3:
                return successFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

}