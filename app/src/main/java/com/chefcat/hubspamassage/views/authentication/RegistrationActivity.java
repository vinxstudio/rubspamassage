package com.chefcat.hubspamassage.views.authentication;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;

import com.chefcat.hubspamassage.R;
import com.chefcat.hubspamassage.views.authentication.adapters.RegistrationAdapter;
import com.chefcat.hubspamassage.views.fragments.AddImagesFragment;
import com.chefcat.hubspamassage.views.fragments.AddInformationFragment;
import com.chefcat.hubspamassage.views.fragments.ConfirmationFragment;
import com.chefcat.hubspamassage.views.fragments.SuccessFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationActivity extends AppCompatActivity {
    public ViewPager mPager;
    public RegistrationAdapter registrationAdapter;

    private AddImagesFragment addImagesFragment;
    private AddInformationFragment addInformationFragment;
    private ConfirmationFragment confirmationFragment;
    private SuccessFragment successFragment;

    @OnClick(R.id.back)
    public void back(){
        finish();
    }

    public void moveToFragment(int position){
        mPager.setCurrentItem(position);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        ButterKnife.bind(this);

        addInformationFragment = new AddInformationFragment();
        addImagesFragment = new AddImagesFragment();
        confirmationFragment = new ConfirmationFragment();
        successFragment = new SuccessFragment();

        registrationAdapter = new RegistrationAdapter(getSupportFragmentManager());
        registrationAdapter.setActivity(this);
        registrationAdapter.setTabs(addInformationFragment, addImagesFragment,
                confirmationFragment, successFragment);

        mPager = (ViewPager) findViewById(R.id.pager_dashboard);

        mPager.setAdapter(registrationAdapter);
        mPager.setOffscreenPageLimit(3);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        mPager.setPageMargin(pageMargin);
        mPager.setCurrentItem(0);

    }


}
