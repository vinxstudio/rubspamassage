package com.chefcat.hubspamassage.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chefcat.hubspamassage.R;

import butterknife.ButterKnife;

/**
 * Created by Knight on 06/08/2016.
 */
public class SuccessFragment extends Fragment {


    public SuccessFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_registration_success, container, false);

        ButterKnife.bind(this, rootView);

        return rootView;
    }

}
