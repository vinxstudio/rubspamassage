package com.chefcat.hubspamassage.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chefcat.hubspamassage.R;
import com.chefcat.hubspamassage.views.authentication.RegistrationActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Knight on 06/08/2016.
 */
public class ConfirmationFragment extends Fragment {

    @OnClick(R.id.btn_next)
    public void goToNext(){
        ((RegistrationActivity) getActivity()).moveToFragment(3);
    }


    public ConfirmationFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_registration_personal_information, container, false);

        ButterKnife.bind(this, rootView);

        return rootView;
    }
}
