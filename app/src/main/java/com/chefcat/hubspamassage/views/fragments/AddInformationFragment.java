package com.chefcat.hubspamassage.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chefcat.hubspamassage.R;
import com.chefcat.hubspamassage.views.authentication.RegistrationActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Knight on 06/08/2016.
 */
public class AddInformationFragment extends Fragment {

    @OnClick(R.id.btn_next)
    public void goToNext(){
        ((RegistrationActivity) getActivity()).moveToFragment(1);
    }

    public AddInformationFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_information, container, false);

        ButterKnife.bind(this, rootView);

        return rootView;
    }
}
