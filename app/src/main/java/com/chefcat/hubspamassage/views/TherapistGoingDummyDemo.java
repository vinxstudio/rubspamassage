package com.chefcat.hubspamassage.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.chefcat.hubspamassage.R;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.maps.MapView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.chefcat.hubspamassage.R.menu.drawer;

public class TherapistGoingDummyDemo extends AppCompatActivity {

    private MapView mapView;

    @BindView(R.id.preparing)
    RelativeLayout preparing;


    @BindView(R.id.feedback)
    RelativeLayout feedback;


    @BindView(R.id.breakdown)
    RelativeLayout breakdown;


    @BindView(R.id.extension)
    RelativeLayout extension;

    @BindView(R.id.timesup)
    RelativeLayout timesup;


    @BindView(R.id.ready)
    RelativeLayout ready;



    @OnClick(R.id.btn_next)
    public void goToPreparing() {
        preparing.setVisibility(RelativeLayout.VISIBLE);
        btnNext.setVisibility(View.GONE);
        getSupportActionBar().setTitle("Massage Session");
    }


    @OnClick(R.id.preparing)
    public void goToReady(){
        preparing.setVisibility(RelativeLayout.GONE);
        ready.setVisibility(RelativeLayout.VISIBLE);
        getSupportActionBar().setTitle("Massage Session");
    }


    @OnClick(R.id.ready)
    public void goToStart(){
        ready.setVisibility(RelativeLayout.GONE);
        go.setVisibility(RelativeLayout.VISIBLE);
        getSupportActionBar().setTitle("Massage in Session");
    }

    @OnClick(R.id.btn_continue)
    public void goToBack(){
        timesup.setVisibility(RelativeLayout.GONE);
        go.setVisibility(RelativeLayout.VISIBLE);
        getSupportActionBar().setTitle("Massage in Session");
    }


    @OnClick(R.id.go)
    public void goToTimesUp(){
        go.setVisibility(RelativeLayout.GONE);
        timesup.setVisibility(RelativeLayout.VISIBLE);
        getSupportActionBar().setTitle("Massage in Session");
    }

    @OnClick(R.id.btn_end)
    public void goToBreakDown2(){
        go.setVisibility(RelativeLayout.GONE);
        breakdown.setVisibility(RelativeLayout.VISIBLE);
        getSupportActionBar().setTitle("Massage Ended");
    }


    @OnClick(R.id.btn_confirm)
    public void goToBreakDown(){
        extension.setVisibility(RelativeLayout.GONE);
        go.setVisibility(RelativeLayout.VISIBLE);
        getSupportActionBar().setTitle("Massage In Session");
    }


    @OnClick(R.id.btn_feedback)
    public void goToFeedback(){
        breakdown.setVisibility(RelativeLayout.GONE);
        feedback.setVisibility(RelativeLayout.VISIBLE);
        getSupportActionBar().setTitle("Feedback");
    }




    @OnClick(R.id.btn_finish)
    public void done(){
        Intent i  = new Intent(TherapistGoingDummyDemo.this, MainDrawerActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }



    @BindView(R.id.go)
    RelativeLayout go;

    @BindView(R.id.btn_next)
    Button btnNext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Mapbox.getInstance(this, getString(R.string.access_token));
        setContentView(R.layout.activity_coming);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);
        mapView = (MapView) findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }



}
