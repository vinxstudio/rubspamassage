package com.chefcat.hubspamassage.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.chefcat.hubspamassage.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class TherapistsListActivity extends AppCompatActivity {

    @OnClick(R.id.one)
    public void one(){
        goToTimer();
    }

    @OnClick(R.id.two)
    public void two(){
        goToTimer();
    }

    @OnClick(R.id.three)
    public void three(){
        goToTimer();
    }

    @OnClick(R.id.four)
    public void four(){
        goToTimer();
    }

    public void goToTimer(){
        Intent i = new Intent(TherapistsListActivity.this, WaitingActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_therapists);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
