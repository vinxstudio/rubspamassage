package com.chefcat.hubspamassage.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

import com.chefcat.hubspamassage.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookMassageActivity extends AppCompatActivity {

    @OnClick(R.id.back)
    public void back(){
        finish();
    }


    @BindView(R.id.choice1)
    RelativeLayout choice1;
    @BindView(R.id.choice2)
    RelativeLayout choice2;
    @BindView(R.id.choice3)
    RelativeLayout choice3;

    @BindView(R.id.choice11)
    RelativeLayout choice11;
    @BindView(R.id.choice12)
    RelativeLayout choice12;
    @BindView(R.id.choice13)
    RelativeLayout choice13;
    @BindView(R.id.choice14)
    RelativeLayout choice14;
    @BindView(R.id.choice15)
    RelativeLayout choice15;


    @OnClick(R.id.choice1)
    public void action1(){
        resetTopChoice();
        choice1.setBackgroundColor(getResources().getColor(R.color.fbutton_color_orange_dark));
    }
    @OnClick(R.id.choice2)
    public void action2(){
        resetTopChoice();
        choice2.setBackgroundColor(getResources().getColor(R.color.fbutton_color_orange_dark));
    }
    @OnClick(R.id.choice3)
    public void action3(){
        resetTopChoice();
        choice3.setBackgroundColor(getResources().getColor(R.color.fbutton_color_orange_dark));
    }

    @OnClick(R.id.choice11)
    public void action11(){
        resetBottomChoice();
        choice11.setBackgroundColor(getResources().getColor(R.color.fbutton_color_orange_dark));
    }

    @OnClick(R.id.choice12)
    public void action12(){
        resetBottomChoice();
        choice12.setBackgroundColor(getResources().getColor(R.color.fbutton_color_orange_dark));
    }

    @OnClick(R.id.choice13)
    public void action13(){
        resetBottomChoice();
        choice13.setBackgroundColor(getResources().getColor(R.color.fbutton_color_orange_dark));
    }

    @OnClick(R.id.choice14)
    public void action14(){
        resetBottomChoice();
        choice14.setBackgroundColor(getResources().getColor(R.color.fbutton_color_orange_dark));
    }

    @OnClick(R.id.choice15)
    public void action15(){
        resetBottomChoice();
        choice15.setBackgroundColor(getResources().getColor(R.color.fbutton_color_orange_dark));
    }

    public void resetTopChoice(){
        choice1.setBackground(getResources().getDrawable(R.drawable.border_bg_gray_nocurve));
        choice2.setBackground(getResources().getDrawable(R.drawable.border_bg_gray_nocurve));
        choice3.setBackground(getResources().getDrawable(R.drawable.border_bg_gray_nocurve));
    }

    public void resetBottomChoice(){
        choice11.setBackground(getResources().getDrawable(R.drawable.border_bg_gray_nocurve));
        choice12.setBackground(getResources().getDrawable(R.drawable.border_bg_gray_nocurve));
        choice13.setBackground(getResources().getDrawable(R.drawable.border_bg_gray_nocurve));
        choice14.setBackground(getResources().getDrawable(R.drawable.border_bg_gray_nocurve));
        choice15.setBackground(getResources().getDrawable(R.drawable.border_bg_gray_nocurve));
    }


    @OnClick(R.id.btn_next)
    public void goToTherapist() {
        Intent i = new Intent(BookMassageActivity.this, TherapistsListActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bookmassage);

        ButterKnife.bind(this);
    }


}
