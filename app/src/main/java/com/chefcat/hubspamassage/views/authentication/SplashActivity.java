package com.chefcat.hubspamassage.views.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;

import com.chefcat.hubspamassage.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends AppCompatActivity {


    @BindView(R.id.selection)
    public LinearLayout selection;

    @OnClick(R.id.register)
    public void register(){
        Intent i = new Intent(SplashActivity.this, RegistrationActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    @OnClick(R.id.signin)
    public void signIn(){
        Intent i = new Intent(SplashActivity.this, SigninActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        Log.e("Debug", "a");
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                proceed();
            }
        }, 2000);
    }

    public void proceed() {
        selection.setVisibility(LinearLayout.VISIBLE);
    }
}
