package com.chefcat.hubspamassage.views;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefcat.hubspamassage.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WaitingActivity extends AppCompatActivity {

    @BindView(R.id.time)
    TextView time;

    @OnClick(R.id.time)
    public void goToTravelling(){
        Intent i = new Intent(WaitingActivity.this, TherapistGoingDummyDemo.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    @BindView(R.id.timelayout)
    LinearLayout container;

    @BindView(R.id.timeout)
    RelativeLayout out;



    public boolean timer = true;

    @OnClick(R.id.yes)
    public void yes(){

        container.setVisibility(LinearLayout.VISIBLE);
        out.setVisibility(RelativeLayout.GONE);
        waitingTime = 30;
        new CountDownTimer(32000, 1000) {
            public void onTick(long millisUntilFinished) {
                time.setText("" + waitingTime--);
            }
            public void onFinish() {
                if(timer) {
                    container.setVisibility(LinearLayout.GONE);
                    out.setVisibility(RelativeLayout.VISIBLE);
                }
            }
        }.start();
    }

    @OnClick(R.id.no)
    public void no(){
        finish();
    }

    int waitingTime = 30;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        new CountDownTimer(32000, 1000) {

            public void onTick(long millisUntilFinished) {
                time.setText("" + waitingTime--);
            }

            public void onFinish() {

                if(timer) {
                    container.setVisibility(LinearLayout.GONE);
                    out.setVisibility(RelativeLayout.VISIBLE);
                }
            }

        }.start();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
