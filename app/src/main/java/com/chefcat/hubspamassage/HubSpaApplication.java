package com.chefcat.hubspamassage;

/**
 * Created by Wien on 1/14/2016.
 */

import android.app.Application;
import android.content.Context;

import com.chefcat.hubspamassage.utils.TypefaceUtil;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class HubSpaApplication extends Application {

    private static HubSpaApplication mInstance;
//    public static BoardwalkAPIService mNetworkService;

//    ImageLoader imageLoader;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/PoiretOne-Regular.ttf");

//        Fabric.with(this, new Crashlytics());
//
//        Mapbox.getInstance(this, getString(R.string.access_token));
//
//        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/gs.ttf");
//
//        Realm.init(this);
////        imageLoader = ImageLoader.getInstance();
////        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
//        mNetworkService = ServiceGenerator.createService(BoardwalkAPIService.class, getApplicationContext());

    }

    public static synchronized HubSpaApplication getInstance() {
        return mInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}